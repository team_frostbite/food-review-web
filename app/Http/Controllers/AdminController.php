<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_pages/home');
    }

    public function show_review()
    {

        return view("admin_pages/review");
    }
    public function write_a_review()
    {

        return view("admin_pages/write_a_review");
    }
    public function offers()
    {

        return view("admin_pages/offers");
    }
    public function share_a_food()
    {

        return view("admin_pages/share_a_food");
    }
    public function foodgraphy()
    {

        return view("admin_pages/foodgraphy");
    }

    public function create_pages()
    {

        return view("admin_pages/create_pages");
    }

}
