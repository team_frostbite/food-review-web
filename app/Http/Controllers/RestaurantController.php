<?php

namespace App\Http\Controllers;

use App\restaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        //
        $restaurants = restaurant::all();
        return view('restaurant.index')->withrestaurants($restaurants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

return view('restaurant.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $restaurant = new restaurant;
        $restaurant->restaurant_name =$request->restaurant_name;
        $restaurant->address =$request->address;


        $restaurant->save();
        return redirect()->route('restaurant.show',$restaurant->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
     $restaurant = restaurant::find($id);
     return view('restaurant.show')->withrestaurant($restaurant);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $restaurant = restaurant::find($id);
        return view('restaurant.edit')->withrestaurant($restaurant);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $restaurant = restaurant::find($id);
        $restaurant->restaurant_name= $request->input('restaurant_name');
        $restaurant->address= $request->input('address');
        $restaurant->save();
        return redirect()->route('restaurant.show',$restaurant->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $restaurant = restaurant::find($id);
        $restaurant->delete();
        return redirect()->route('restaurant.index');


    }
}
