<?php

namespace App\Http\Controllers;

use App\Shareafood;
use Illuminate\Http\Request;

class ShareafoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'Hello World';

    }

    public function share_a_food()
    {

        return view("pages/share_a_food");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shareafood = new Shareafood();
        $shareafood->restaurant_name = $request->restaurant_name;
        $shareafood->address = $request->address;

        $shareafood->save();
        //echo "haha";
     // dd($shareafood);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shareafood  $shareafood
     * @return \Illuminate\Http\Response
     */
    public function show(Shareafood $shareafood)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shareafood  $shareafood
     * @return \Illuminate\Http\Response
     */
    public function edit(Shareafood $shareafood)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shareafood  $shareafood
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shareafood $shareafood)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shareafood  $shareafood
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shareafood $shareafood)
    {
        //
    }
}
