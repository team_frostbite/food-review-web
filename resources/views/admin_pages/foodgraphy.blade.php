@extends('main_admin')
@section('content')
    <div class="row">
        <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="gallery-title">Gallery</h1>
        </div>

        <div align="center">
            <button class="btn btn-default filter-button" data-filter="all">All</button>
            <button class="btn btn-default filter-button" data-filter="indian">indian</button>
            <button class="btn btn-default filter-button" data-filter="deshi">deshi</button>
            <button class="btn btn-default filter-button" data-filter="chinese">chinese</button>

        </div>
        <br/>



        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter indian">
            <img src="{{URL::asset('/images/aa.jpg')}}" alt="profile Pic" height="300" width="300" class="img-responsive">
        </div>

        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter indian">
            <img src="{{URL::asset('/images/ss.jpg')}}" alt="profile Pic" height="365" width="365" class="img-responsive">
        </div>

        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter deshi">
            <img src="{{URL::asset('/images/dd.jpg')}}" alt="profile Pic" height="365" width="365" class="img-responsive">
        </div>

        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter deshi">
            <img src="{{URL::asset('/images/ff.jpg')}}" alt="profile Pic" height="365" width="365" class="img-responsive">
        </div>

        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter chinese">
            <img src="{{URL::asset('/images/gg.jpg')}}" alt="profile Pic" height="365" width="365" class="img-responsive">
        </div>

        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter chinese">
            <img src="{{URL::asset('/images/hh.jpg')}}" alt="profile Pic" height="365" width="365" class="img-responsive">
        </div>


    </div>
@endsection