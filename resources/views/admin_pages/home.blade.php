@extends('main_admin')
@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h1 class="text-center" id="aa">What are you looking for</h1>
                    <div class="form-group">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group" id="adv-search">
                                        <input type="text" class="form-control" placeholder="Search restaurant,reviews" />
                                        <div class="input-group-btn">
                                            <div class="btn-group" role="group">
                                                <div class="dropdown dropdown-lg">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                        <form class="form-horizontal" role="form">
                                                            <div class="form-group">
                                                                <label for="filter">search by</label>
                                                                <select class="form-control">

                                                                    <option value="1">restaurant</option>
                                                                    <option value="2">review</option>

                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contain">Place</label>
                                                                <input class="form-control" type="text" />
                                                            </div>

                                                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
        <div class="container">

        <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Trending Foods</h1></div>
        <div class="col-md-4">
            <div class="thumbnail"><img>
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail"><img>
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail"><img>
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Trending Places</h1></div>
        <div class="col-md-4">
            <div class="thumbnail"><img>
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail"><img>
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail"><img>
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>

        </div>
    </div>
</div>

    @endsection