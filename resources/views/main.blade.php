<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BdFoodReview</title>
    <link href="{{ asset('css/bootstrap.min.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/Pretty-Footer.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/styles.css')}}" media="all" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/signup-popup.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/untitled.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('fonts/font-awesome.min.css')}}" media="all" rel="stylesheet" type="text/css" />




    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bree+Serif">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Permanent+Marker">


</head>

<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header"><a class="navbar-brand navbar-link" href="/home">BdFoodReview </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav navbar-right"></ul>
            <ul class="nav navbar-nav">

                <li role="presentation"><a href="/reviews">Reviews </a></li>
                <li role="presentation"><a href="/offers">Offers </a></li>
                <li role="presentation"><a href="/share_a_food">Share A Food</a></li>
                <li role="presentation"><a href="/foodgraphy">FoodGraphy </a></li>
            </ul>

        <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest


                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>

                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                    @endguest

            </ul>

        </div>
    </div>
</nav>

<div class="container">
               @yield('content')
        </div>
        <div class="col-md-12">
            <footer>
                <div class="row">
                    <div class="col-md-6 col-sm-6 footer-navigation">
                        <h3><a href="#">BdFoodReview </a></h3>
                        <p class="links"><a href="#">Home</a><strong> · </strong><a href="#">Blog</a><strong> · </strong><a href="#">About</a><strong> · </strong><a href="#">Faq</a><strong> · </strong><a href="#">Contact</a></p>
                        <p class="company-name">Bdfoodreview© 2017 </p>
                    </div>
                    <div class="clearfix visible-sm-block"></div>
                    <div class="col-md-6 footer-about">
                        <h4>About the company</h4>
                        <p> Bdfoodreview is the foodreview and rating website where you can find and search the foods and restaurants in Dhaka city.
                        </p>
                        <div class="social-links social-icons">
                            <div class="col-md-6 col-md-offset-3"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><a href="#"><i class="fa fa-github"></i></a></div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.2.1.js"
        integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript" src="{{ URL::asset('js/lala.js') }}"></script>

</body>

</html>