<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BdFoodReview</title>
    <link href="{{ asset('css/bootstrap.min.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/Pretty-Footer.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/styles.css')}}" media="all" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/signup-popup.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/untitled.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('fonts/font-awesome.min.css')}}" media="all" rel="stylesheet" type="text/css" />




    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bree+Serif">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Permanent+Marker">


</head>

<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header"><a class="navbar-brand navbar-link" href="/admin_home">BdFoodReview </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav navbar-right"></ul>
            <ul class="nav navbar-nav">

                <li role="presentation"><a href="/admin_review">Reviews </a></li>
                <li role="presentation"><a href="/admin_offers">Offers </a></li>
                <li role="presentation"><a href="/admin_share_a_food">Share A Food</a></li>
                <li role="presentation"><a href="/admin_foodgraphy">FoodGraphy </a></li>
                <li role="presentation"><a href="/admin_create">create </a></li>
            </ul>
           {{-- <a class="btn btn-link navbar-btn navbar-right" role="button" href="#">
                <!-- Large modal -->
                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    SignUp/Login</button>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×</button>
                                <h4 class="modal-title" id="myModalLabel">
                                    Login/Registration - <a href="http://www.jquery2dotnet.com"></a></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#Login" data-toggle="tab">Login</a></li>
                                            <li><a href="#Registration" data-toggle="tab">Registration</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="Login">
                                                <form role="form" class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-2 control-label">
                                                            Email</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" class="form-control" id="email1" placeholder="Email" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                                            Password</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Password" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <button type="submit" class="btn btn-primary btn-sm">
                                                                Submit</button>
                                                            <a href="javascript:;">Forgot your password?</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane" id="Registration">
                                                <form role="form" class="form-horizontal">
                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-2 control-label">
                                                            Name</label>
                                                        <div class="col-sm-10">
                                                            <div class="row">

                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" placeholder="Name" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-2 control-label">
                                                            Email</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" class="form-control" id="email" placeholder="Email" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="password" class="col-sm-2 control-label">
                                                            Password</label>
                                                        <div class="col-sm-10">
                                                            <input type="password" class="form-control" id="password" placeholder="Password" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <button type="button" class="btn btn-primary btn-sm">
                                                                Save & Continue</button>
                                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                                Cancel</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div id="OR" class="hidden-xs">
                                            OR</div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row text-center sign-with">
                                            <div class="col-md-12">
                                                <h3>
                                                    Sign in with</h3>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="btn-group btn-group-justified">
                                                    <a href="#" class="btn btn-primary" id="kola">Facebook</a> <a href="#" class="btn btn-danger">
                                                        Google</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </a>--}}

        <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest


                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>

                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                    @endguest

            </ul>

        </div>
    </div>
</nav>

<div class="container">
               @yield('content')
        </div>
        <div class="col-md-12">
            <footer>
                <div class="row">
                    <div class="col-md-6 col-sm-6 footer-navigation">
                        <h3><a href="#">BdFoodReview </a></h3>
                        <p class="links"><a href="#">Home</a><strong> · </strong><a href="#">Blog</a><strong> · </strong><a href="#">About</a><strong> · </strong><a href="#">Faq</a><strong> · </strong><a href="#">Contact</a></p>
                        <p class="company-name">Bdfoodreview© 2017 </p>
                    </div>
                    <div class="clearfix visible-sm-block"></div>
                    <div class="col-md-6 footer-about">
                        <h4>About the company</h4>
                        <p> Bdfoodreview is the foodreview and rating website where you can find and search the foods and restaurants in Dhaka city.
                        </p>
                        <div class="social-links social-icons">
                            <div class="col-md-6 col-md-offset-3"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><a href="#"><i class="fa fa-github"></i></a></div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.2.1.js"
        integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript" src="{{ URL::asset('js/lala.js') }}"></script>

</body>

</html>