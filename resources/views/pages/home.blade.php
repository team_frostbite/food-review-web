@extends('main')
@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <h1 class="text-center" id="aa">What are you looking for</h1>
                    <div class="form-group">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group" id="adv-search">
                                        <input type="text" class="form-control" placeholder="Search restaurant,reviews" />
                                        <div class="input-group-btn">
                                            <div class="btn-group" role="group">
                                                <div class="dropdown dropdown-lg">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                        <form class="form-horizontal" role="form">
                                                            <div class="form-group">
                                                                <label for="filter">search by</label>
                                                                <select class="form-control">

                                                                    <option value="1">restaurant</option>
                                                                    <option value="2">review</option>

                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contain">Place</label>
                                                                <input class="form-control" type="text" />
                                                            </div>

                                                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
        <div class="container">

        <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Trending Foods</h1></div>
        <div class="col-md-4">
            <div class="thumbnail"> <img src="{{URL::asset('/images/food.jpg')}}" alt="profile Pic" height="200" width="200 " class="img-responsive">
                <div class="caption">
                    <h3>pizza</h3>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>
            <div class="col-md-4">
                <div class="thumbnail"> <img src="{{URL::asset('/images/gg.jpg')}}" alt="profile Pic" height="200" width="200 " class="img-responsive">
                    <div class="caption">
                        <h3>Gamblers mix</h3>
                    </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
            </div>
            <div class="col-md-4">
                <div class="thumbnail"> <img src="{{URL::asset('/images/rr.jpg')}}" alt="profile Pic" height="200" width="200 " class="img-responsive">
                    <div class="caption">
                        <h3>Special Swarma</h3>
                    </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
            </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Trending Places</h1></div>
        <div class="col-md-4">
            <div class="thumbnail"> <img src="{{URL::asset('/images/32.jpg')}}" alt="profile Pic" height="200" width="200 " class="img-responsive">
                <div class="caption">
                    <h3>Sky view</h3>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail"> <img src="{{URL::asset('/images/44.jpg')}}" alt="profile Pic" height="200" width="200 " class="img-responsive">
                <div class="caption">
                    <h3>Amari</h3>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail"> <img src="{{URL::asset('/images/3.jpg')}}" alt="profile Pic" height="200" width="200 " class="img-responsive">
                <div class="caption">
                    <h3>Royal Buffet</h3>
                </div><a class="btn btn-primary btn-block" role="button" href="#">Button</a></div>
        </div>

        </div>
    </div>
</div>

    @endsection