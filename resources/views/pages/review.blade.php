@extends('main')
@section('content')

    <h1 class="text-center text-warning">Reviews</h1>

    <div class="container" >
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-2">
            <a href="/reviews/create" class="btn btn-info" role="button">Write a review</a>
            </div>

            </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <table class="table">

                <thead>
                <th>reviewer</th>
                <th>restaurant name</th>
                <th>item name</th>
                <th>rating</th>
                <th>review</th>

                </thead>
                <tbody>

                  <td>ayon</td>
                  <td>kfc</td>
                  <td>chicken</td>
                  <td>6</td>
                  <td>average</td>


                </tbody>
                <tbody>

                <td>smita</td>
                <td>bfc</td>
                <td>coleslaw</td>
                <td>8</td>
                <td>good</td>


                </tbody>
                <tbody>

                <td>hridima</td>
                <td>tune&bites</td>
                <td>jazz lovers meal</td>
                <td>9</td>
                <td>very good</td>


                </tbody>
            </table>


        </div>

    </div>
    {{--<div class="container" >
        <div class="text-center">
        <a href="/write_a_review" class="btn btn-info" role="button">Write a review</a>  </div>  </div>
--}}
    @endsection