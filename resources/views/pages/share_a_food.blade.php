@extends('main')
@section('content')


    <h1 class="text-center text-danger">Share A Food</h1></div>
    <div class="container">
        <form method="POST" action="{{ route('shareafood.store') }}">
            {{ csrf_field() }}




            <div class="form-group">
                <label name="restaurant_name">Restaurant name:</label>
                <input id="restaurant_name" name="restaurant_name" class="form-control">
            </div>
            <div class="form-group">
                <label name="address">Address:</label>
                <input id="address" name="address" rows="10" class="form-control"></input>
            </div>

            <input type="submit" value="Create Post" class="btn btn-success btn-lg btn-block">
            <input type="hidden" name="_token" value="{{ Session::token() }}">
            </div>
    </form>


    @endsection