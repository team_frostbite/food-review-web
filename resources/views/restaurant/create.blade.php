@extends('main_admin')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Create New Restaurant</h1>
            <hr>
            <form method="POST" action="{{ route('restaurant.store') }}">
                <div class="form-group">
                    <label name="restaurant_name">Restaurant name:</label>
                    <input id="restaurant_name" name="restaurant_name" class="form-control">
                </div>
                <div class="form-group">
                    <label name="address">Address:</label>
                    <textarea id="address" name="address" rows="10" class="form-control"></textarea>
                </div>
                <input type="submit" value="Create Post" class="btn btn-success btn-lg btn-block">
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>
    </div>﻿

    @endsection