@extends('main_admin')

@section('content')

    <div class="row">
       <div class="col-md-10">

           <h1>all post</h1>

       </div>
        <div class="col-md-2">

            <a href="{{ route('restaurant.create') }}" class="btn btn-primary btn-block">create restaurant</a>

        </div>

<div class="row">
    <div class="col-md-12">
        <table class="table">

            <thead>
            <th>#</th>
            <th>restaurant name</th>
            <th>address</th>
            <th>created at</th>

            </thead>
<tbody>
  @foreach($restaurants as $restaurant)
<tr>
    <th>{{$restaurant->id}}</th>
    <td>{{$restaurant->restaurant_name}}</td>
    <td>{{str_limit($restaurant->address, 20)}}</td>
    <td>{{date('M j, Y',strtotime($restaurant->created_at))}}</td>
    <td>  <div class="row">
            <div class="col-sm-4">
                <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-primary btn-block">Show</a>
            </div>
            <div class="col-sm-4">
                <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
            </div>
            <div class="col-sm-4">
                {{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}
                <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{ method_field('DELETE') }}
                </form>﻿
            </div>
        </div>﻿</td>
</tr>
@endforeach

</tbody>
        </table>


    </div>

</div>

    </div>﻿

    @endsection