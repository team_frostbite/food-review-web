@extends('main_admin')

@section('content')

    <form method="POST" action="{{ route('restaurant.update', $restaurant->id) }}">
        <div class="form-group">
            <label for="restaurant_name">restaurant_name:</label>
            <textarea type="text" class="form-control input-lg" id="restaurant_name" name="restaurant_name" rows="1" style="resize:none;">{{ $restaurant->restaurant_name }}</textarea>
        </div>
        <div class="form-group">
            <label for="address">address:</label>
            <textarea type="text" class="form-control input-lg" id="address" name="address" rows="10">{{ $restaurant->address }}</textarea>
        </div>

        <div class="col-md-4">
            <div class="well">
                <dl class="dl-horizontal">
                    <dt>Created at:</dt>
                    <dd>{{ date('M j, Y h:i:sa', strtotime($restaurant->created_at)) }}</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Last updated:</dt>
                    <dd>{{ date('M j, Y h:i:sa', strtotime($restaurant->updated_at)) }}</dd>
                </dl>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ route('restaurant.show', $restaurant->id) }}" class="btn btn-danger btn-block">Back</a>
                    </div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success btn-block">Save</button>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
        {{ method_field('PUT') }}

                    </div>
                </div>
            </div>
        </div>

    </form>﻿
    @endsection