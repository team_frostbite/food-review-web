@extends('main_admin')

@section('content')

    <div class="row">
        <div class="col-md-8 ">
            <h1>{{$restaurant->restaurant_name}}</h1>
            <p class="form-control">{{$restaurant->address}} </p>
        </div>
    </div>﻿


        <div class="row">
            <div class="col-sm-4">
                <a href="{{ route('restaurant.edit', $restaurant->id) }}" class="btn btn-primary btn-block">Edit</a>
            </div>
            <div class="col-sm-4">
             {{--   <a href="{{ route('restaurant.destroy', $restaurant->id) }}" class="btn btn-danger btn-block">Delete</a>--}}
                <form method="POST" action="{{ route('restaurant.destroy', $restaurant->id) }}">
                    <input type="submit" value="Delete" class="btn btn-danger btn-block">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    {{ method_field('DELETE') }}
                </form>﻿
            </div>
        </div>﻿







    @endsection