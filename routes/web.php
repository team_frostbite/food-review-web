<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/vue', function () {
    return view('welcome');
});


Route::get('/home','PostsController@index');
//Route::get('/review','PostsController@show_review');
//Route::get('/write_a_review','PostsController@write_a_review')->middleware('auth');
Route::get('/offers','PostsController@offers');
Route::get('/share_a_food','ShareafoodController@share_a_food')->middleware('auth');
Route::get('/foodgraphy','PostsController@foodgraphy');


Route::resource('post','PostsController');
Route::resource('restaurant','RestaurantController');
Route::resource('shareafood','ShareafoodController');
Route::resource('reviews','ReviewsController');


Route::get('/admin_review','AdminController@show_review');
Route::get('/admin_write_a_review','AdminController@write_a_review');
Route::get('/admin_offers','AdminController@offers');
Route::get('/admin_share_a_food','AdminController@share_a_food');
Route::get('/admin_foodgraphy','AdminController@foodgraphy');
Route::get('/admin_home','AdminController@index');
Route::get('/admin_create','AdminController@create_pages');




Auth::routes();

/*Route::prefix('admin')->group(function (){
    Route::get('/login','/Auth/AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login','/Auth/AdminLoginController@login')->name('admin.login.submit');
    Route::get('/','AdminController@index')->name('admin.dashboard');

});*/
Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
});

//Route::get('/home', 'HomeController@index')->name('home');
